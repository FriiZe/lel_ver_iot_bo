import React, { Component } from 'react';
import axios from 'axios';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee, faCircle  } from '@fortawesome/free-solid-svg-icons';

const apiUrl = "http://localhost:8080";

class StreetMap extends Component {
    constructor() {
        super()
        this.state = {
            //coordinates at the center of bordeaux
            bordeaux: {
                lat: 44.837788,
                lng: -0.579180,
                zoom: 10,
            },
            devices: []
        }
    }

    async fetchDevices() {
        const res = await axios.get(apiUrl + '/devices');
        this.setState({
            devices: res.data
        })
    }

    componentDidMount() {
        this.fetchDevices();
    }

    render() {

        const position = [this.state.bordeaux.lat, this.state.bordeaux.lng];

        return (
            <Map center={position} zoom={this.state.bordeaux.zoom} style={{ width: '100%', height: '100vh' }}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />
                {this.state.devices.map((row) => {
                    const pos = row.zone.split(',');

                    return (
                        <Marker
                            key={row.id}
                            position={[pos[0], pos[1]]}
                        >
                            <Popup>
                                <div>
                                    <h2>
                                        {row.id} - &nbsp;
                                        {row.status === 'UP' ? (<FontAwesomeIcon icon={faCircle} color="green"/>) : (<FontAwesomeIcon icon={faCoffee} color="red"/>)}
                                    </h2>
                                    <p>Température: {row.temperature[row.temperature.length - 1]}°C</p>
                                    <p>Humidité: {row.humidity[row.humidity.length - 1]}%</p>
                                    <p>Vent: {row.wind[row.wind.length - 1]}(km/h)</p>
                                </div>
                            </Popup>
                        </Marker>
                    )
                })}
            </Map>
        );
    }
}

export default StreetMap;