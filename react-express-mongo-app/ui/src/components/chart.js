import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import {
  ArgumentAxis,
  ValueAxis,
  Chart,
  LineSeries,
} from '@devexpress/dx-react-chart-material-ui';

const apiUrl = "http://localhost:8080";


class Charts extends React.Component {
    state = {
        data: [],
        type: null,
        device: null,
        list: []
    }

    componentDidMount() {
        this.fetchDevicesNames()
        // this.fetch()
    }

    async fetchDevicesNames() {
        const res = await axios.get(apiUrl + '/devices/name')
        this.setState({
            list: res.data
        })
    }

    async fetch(id, type) {
      const res = await axios.get(apiUrl + `/device/${id}`);
      console.log(res.data)
      let datas = []
      switch (type) {
        case 'temperature':
          for(let i = 0; i < res.data.temperature.length; i++) {
              datas.push({argument: i, value: res.data.temperature[i]})
          } break;
        case 'wind':
          for(let i = 0; i < res.data.wind.length; i++) {
              datas.push({argument: i, value: res.data.wind[i]})
          } break;
        case 'humidity':
          for(let i = 0; i < res.data.humidity.length; i++) {
              datas.push({argument: i, value: res.data.humidity[i]})
          } break;
        default:
          break;
      }
      this.setState({
          data: datas
      })
    }

    async handleChangeType(e){
        this.setState({ type:e.target.value });
    }

    handleChangeDevice(e){
        this.setState({ device:e.target.value });
    }


    render(){
        return (
            <div>
                <select 
                value={this.state.type} 
                onChange={this.handleChangeType.bind(this)} 
                >
                <option value="">--Choississez une option--</option>
                <option value="temperature">Temperature</option>
                <option value="wind">Wind</option>
                <option value="humidity">Humidity</option>
                </select>
                <select 
                value={this.state.device} 
                onChange={this.handleChangeDevice.bind(this)} 
                >
                <option value="">--Choississez une option--</option>
                {this.state.list.map((item) => (
                <option value={item}>{item}</option>
                ))}
                </select>
                <button onClick={() => {
                  if(this.state.device && this.state.type){
                    this.fetch(this.state.device, this.state.type)
                  }
                }}>Actualiser le graphique</button>
                <Paper>
                    <Chart
                    data={this.state.data}
                    >
                    <ArgumentAxis />
                    <ValueAxis />
        
                    <LineSeries valueField="value" argumentField="argument" />
                    </Chart>
                </Paper>
            </div>
        )
    }
}

export default Charts

