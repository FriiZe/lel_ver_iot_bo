import React, { Component } from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee, faCircle  } from '@fortawesome/free-solid-svg-icons';

const apiUrl = "http://localhost:8080";

class Tableau extends Component {
  state = {
    devices: []
  };

  async fetch() {
    const res = await axios.get(apiUrl + '/devices');
    this.setState({
      devices: res.data
    })
  }

  async fetchOne(device) {
    await axios.get(apiUrl + `/device/${device}`).then((res) => {
      console.log(res.data)
    })
  }

  async switchDevice(device) {
    let res = await axios.get(apiUrl + `/switch/${device}`)
    return res.data
  }

  numAverage(array) {
    let total = 0
    for(let i = 0; i < array.length; i++) {
      total += array[i]
    }
    return total/array.length
  }

  componentDidMount() {
    this.fetch();
  }

  render() {
    const classes = makeStyles({
      table: {
        minWidth: 650,
      },
    });
    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Devices</TableCell>
              <TableCell align="right">Zone</TableCell>
              <TableCell align="right">Degres&nbsp;(°C)</TableCell>
              <TableCell align="right">Degres&nbsp;(Moyenne)</TableCell>
              <TableCell align="right">Humidity&nbsp;(%)</TableCell>
              <TableCell align="right">Humidity&nbsp;(Moyenne)</TableCell>
              <TableCell align="right">Wind&nbsp;(km/h)</TableCell>
              <TableCell align="right">Wind&nbsp;(Moyenne)</TableCell>
              <TableCell align="right">Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.devices.map((row) => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell align="right">{row.zone}</TableCell>
                <TableCell align="right">{row.temperature.join(', ')}</TableCell>
                <TableCell align="right">{this.numAverage(row.temperature)}</TableCell>
                <TableCell align="right">{row.humidity.join(', ')}</TableCell>
                <TableCell align="right">{this.numAverage(row.humidity)}</TableCell>
                <TableCell align="right">{row.wind.join(', ')}</TableCell>
                <TableCell align="right">{this.numAverage(row.wind)}</TableCell>
                <TableCell align="right">{row.status === 'UP' ? (<FontAwesomeIcon icon={faCircle} color="green"/>) : (<FontAwesomeIcon icon={faCoffee} color="red"/>)}</TableCell>
                <button onClick={async () => {
                  let res = await this.switchDevice(row.id)
                  let index = this.state.devices.indexOf(row)
                  this.setState((prevState) => {
                    const devices = [...prevState.devices]
                    devices[index] = {...devices[index], status: res}
                    return {devices}
                  })
                  }}>Change status</button>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default Tableau;