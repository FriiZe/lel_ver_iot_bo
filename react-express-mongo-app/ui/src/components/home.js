import React, { Component } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

import dataDevice from './../assets/grid.png';
import mapDevice from './../assets/map.png';
import graphDevice from './../assets/graph.png';
import { Link } from '@material-ui/core';
import { red } from '@material-ui/core/colors';

import './../css/home.css';

class Home extends Component {

   render() {

      return (
         <div>
            <div>
               <Typography variant="subtitle1" gutterBottom>
                  <div className="presentation">
                     Ce projet a été réalisé par Romain Vergé et Laëtitia Leoni, étudiants à Ynov, dans le cadre du projet en IoT.
                     <br/>
                     Vous trouverez l'énoncé à l'adresse suivante: 
                     <br/>
                     <Link>http://52.14.112.188:3000/</Link>
                     <br/>
                     Le code source du projet se trouve sur le lien suivant: 
                     <br/>
                     <Link>https://gitlab.com/FriiZe/lel_ver_iot_bo</Link>
                     <br/>
                  </div>
               </Typography>
               <Grid container spacing={3}>
                  <Grid item xs={12}>
                     <h1>Tableau de données</h1>
                  </Grid>
                  <Grid item xs={6}>
                     <Container maxWidth="sm" className="imgContent">
                     <img src={dataDevice} alt="icone tableau de données"></img>
                     </Container>
                  </Grid>
                  <Grid item xs={6}>
                     <div className="textContent">
                        Suivi des données des différents devices:
                        <ul>
                           <li>températures (jusqu'à 20 - en °C)</li>
                           <li>moyenne des températures stockées</li>
                           <li>humidité (jusqu'à 20 - en %)</li>
                           <li>moyenne des humidités stockées</li>
                           <li>vent (jusqu'à 20 - en km/h)</li>
                           <li>moyenne sur la puissance du vent</li>
                           <li>statut actuel du device</li>
                           <li>activer/désactiver le statut de chaque device</li>
                        </ul>
                     </div>
                  </Grid>
                  <Grid item xs={12}>
                     <h1>Carte de devices</h1>
                  </Grid>
                  <Grid item xs={6}>
                     <Container maxWidth="sm" className="imgContent">
                     <img src={mapDevice} alt="icone carte de devices"></img>
                     </Container>
                  </Grid>
                  <Grid item xs={6}>
                     <div className="textContent">
                        Affiche sur une carte:
                        <ul>
                           <li>emplacement du device</li>
                           <li>statut</li>
                           <li>température</li>
                           <li>humidité</li>
                           <li>vent</li>
                        </ul>
                     </div>
                  </Grid>
                  <Grid item xs={12}>
                     <h1>Évolution des devices</h1>
                  </Grid>
                  <Grid item xs={6}>
                     <Container maxWidth="sm" className="imgContent">
                     <img src={graphDevice} alt="icone evolution des devices"></img>
                     </Container>
                  </Grid>
                  <Grid item xs={6}>
                     <div className="textContent">
                        Graphique de données selon le device sélectionné et la catégorie souhaitée:
                        <ul>
                           <li>température</li>
                           <li>humidité</li>
                           <li>vent</li>
                        </ul>
                     </div>
                  </Grid>
               </Grid>
            </div>
         </div>
       )
   }
}
export default Home;