import React, { Component } from 'react';
import Tableau from './components/tab'
import StreetMap from './components/map'
import Home from './components/home'
import { BrowserRouter, Route, Link } from "react-router-dom";
import './App.css';
import Charts from './components/chart'

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <div className="nav">
            <ul className="menuUl">
              <li className="menuLi">
                <Link to="/">Accueil</Link>
              </li>
              <li className="menuLi">
                <Link to="/tableau">Tableau de données</Link>
              </li>
              <li className="menuLi">
                <Link to="/map">Carte de devices</Link>
              </li>
              <li className="menuLi">
                <Link to="/chart">Evolution des devices</Link>
              </li>
            </ul>
          </div>
          <hr />
          <div className="main-route-place">
            <Route exact path="/" component={Home} />
            <Route path="/tableau" component={Tableau} />
            <Route path="/map" component={StreetMap} />
            <Route path="/chart" component={Charts} />
          </div>
        </div>
      </BrowserRouter>
    )
  }
}


export default App;
