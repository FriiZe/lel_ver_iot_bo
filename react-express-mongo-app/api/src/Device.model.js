const mongoose = require("mongoose");

const deviceSchema = new mongoose.Schema({
  id: {
    type: String
  }, 
  zone: {
      type: String
  }, 
  temperature: {
      type: [Number]
  },
  humidity: {
      type: [Number]
  },
  wind: {
    type: [Number]
  },
  status: {
    type: String
  }
});

const Device = mongoose.model("Device", deviceSchema);

module.exports = Device;
