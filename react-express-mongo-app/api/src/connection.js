const mongoose = require("mongoose");

const Device = require('./Device.model');

const connection = "mongodb://mongo:27017/mongo-test";

const connectDb = () => {
  return mongoose.connect(connection).then(() => {
    return Device.remove({}, function (err){
      console.log('collection removed')
    })
  });
};

module.exports = connectDb;
