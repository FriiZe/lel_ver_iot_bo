const express = require('express');
const app = express();
const connectDb = require('./src/connection');
const Device = require('./src/Device.model');
const cors = require('cors');
const axios = require('axios');

const PORT = 8080;
const APISOURCEURL = "http://52.14.112.188:3000/api/v1"
const HISTORIC = 20
const MINUTES = 1

app.use(cors());

app.get('/devices', async (req, res) => {
  const devices = await Device.find();
  res.json(devices)
})

app.get('/devices/name', async (req, res) => {
  let resp = []
  const devices = await Device.find();
  devices.forEach((device) => {
    resp.push(device.id)
  })
  res.send(resp)
})

app.get('/switch/:device_id', async (req, res) => {
  let device = await Device.findOne({id: req.params.device_id})
  if(device.status === 'UP') {
    await Device.update({id: req.params.device_id}, {status: 'DOWN'})
    res.send('DOWN')
  } else {
    await Device.update({id: req.params.device_id}, {status: 'UP'})
    res.send('UP')
  }
})

app.get('/device/:device_id', async (req, res) => {
  res.json(await Device.findOne({id: req.params.device_id}))
})

app.get('/device/:device_id/temperature', async (req, res) => {
  const resp = await Device.findOne({id: req.params.device_id})
  res.json(resp.temperature)
})

app.get('/device/:device_id/wind', async (req, res) => {
  const resp = await Device.findOne({id: req.params.device_id})
  res.json(resp.wind)
})

app.get('/device/:device_id/humidity', async (req, res) => {
  const resp = await Device.findOne({id: req.params.device_id})
  res.json(resp.humidity)
})

app.listen(PORT, function() {
  console.log(`Listening on ${PORT}`);

  connectDb().then(() => {
    console.log('MongoDb connected');
    let nb;
    try {
      axios.get(APISOURCEURL + '/temperature').then((response) => {
        nb = response.data.temperature.length;
        response.data.temperature.forEach( async (temperature) => {
          const device = new Device({id: temperature.id, zone: temperature.zone, temperature: [], humidity: [], wind: []})
          device.temperature.push(temperature["°C"])
          await device.save().then(() => { console.log('Device created') })
        })
        axios.get(APISOURCEURL + '/humidity').then((response) => {
          response.data.humidity.forEach( async (humidity) => {
            const device = await Device.findOne({id: humidity.id})
            device.humidity.push(humidity["%"])
            await device.save().then(() => { console.log('Device updated humidity(%)') })
          })
          axios.get(APISOURCEURL + '/wind').then((response) => {
            response.data.wind.forEach( async (wind) => {
              const device = await Device.findOne({id: wind.id})
              device.wind.push(wind["km/h"])
              await device.save().then(() => { console.log('Device updated wind(km/h)') })
            })
            for(let i = 1; i <= nb; i++) {
              axios.get(APISOURCEURL + `/ping?device=device_${i}`).then( async (response) => {
                await Device.update({id: `device_${i}`}, {status: response.data.device}).then(() => {console.log('Device updated (status)')})
              })
            }
          })
        })
      }) 
      let the_interval = MINUTES * 60 * 1000;
      setInterval(function() {
        console.log("I am doing my minute check");
        axios.get(APISOURCEURL + '/temperature').then((response) => {
          response.data.temperature.forEach( async (temperature) => {
            let device = await Device.findOne({id: temperature.id})
            if(device.status === 'UP'){
              if(device.temperature.length === HISTORIC){
                device.temperature.shift()
              }
              device.temperature.push(temperature["°C"])
              await device.save().then(() => { console.log('Device updated') })
            }
          })
          axios.get(APISOURCEURL + '/humidity').then((response) => {
            response.data.humidity.forEach( async (humidity) => {
              const device = await Device.findOne({id: humidity.id})
              if(device.status === 'UP'){
                if(device.humidity.length === HISTORIC){
                  device.humidity.shift()
                }
                device.humidity.push(humidity["%"])
                await device.save().then(() => { console.log('Device updated humidity(%)') })
              }
            })
            axios.get(APISOURCEURL + '/wind').then((response) => {
              response.data.wind.forEach( async (wind) => {
                const device = await Device.findOne({id: wind.id})
                if(device.status === 'UP'){
                  if(device.wind.length === HISTORIC){
                    device.wind.shift()
                  }
                  device.wind.push(wind["km/h"])
                  await device.save().then(() => { console.log('Device updated wind(km/h)') })
                }
              })
            })
          })
        })
        // do your stuff here
      }, the_interval);
    } catch (error) {
      console.log('error :' + error.message);
    }
  });
});
