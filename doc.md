# Documentation Projet IOT 

Dans le cadre de notre majeure IOT, nous avons réalisé un backoffice de suivi pour des objets connectés. Ci-joint la documentation de la partie API et de la partie UI.

**IMPORTANT** : Toutes les fonctionnalités listées ci-dessous restent et resteront compatibles dans le cas où un ou plusieurs devices sont ajoutés/supprimés

# Getting Started

Requirements : Docker, docker-compose

``` console
git clone git@gitlab.com:FriiZe/lel_ver_iot_bo.git
cd lel_ver_iot_bo/react-express-mongo-app/
docker-compose up --build
```

Atendre d'avoir : 

``` console
api_1    | Device updated humidity(%)
api_1    | Device updated humidity(%)
api_1    | Device updated humidity(%)
api_1    | Device updated humidity(%)
api_1    | Device updated humidity(%)
api_1    | Device updated humidity(%)
api_1    | Device updated humidity(%)
api_1    | Device updated wind(km/h)
api_1    | Device updated wind(km/h)
api_1    | Device updated wind(km/h)
api_1    | Device updated wind(km/h)
api_1    | Device updated wind(km/h)
api_1    | Device updated wind(km/h)
api_1    | Device updated wind(km/h)
api_1    | Device updated (status)
api_1    | Device updated (status)
api_1    | Device updated (status)
api_1    | Device updated (status)
api_1    | Device updated (status)
api_1    | Device updated (status)
api_1    | Device updated (status)
```

Rendez-vous sur votre navigateur favori à l'adresse : http://localhost

# API

Le serveur a été développé en ExpressJS, surcouche de NodeJS. Concernant la partie base de données, nous utilisons MongoDB. Afin de faciliter le développement ainsi que la compréhension du code, nous avons mis en place Mongoose, un ODM pour MongoDB.

Plusieurs fonctionnalités du serveur sont notables : 

 - Afin de pouvoir travailler dans une BDD propre, nous effaçons la base dès que le seveur se connecte à la base de données :

 ``` javascript 
 const  connectDb = () => {
	return  mongoose.connect(connection).then(() => {
		return  Device.remove({}, function (err){
			console.log('collection removed')
		})
	});
}; 
 ```

- Au lancement du serveur nous récupérons également toutes les données des objets connectés via une API disponible à l'adresse http://52.14.112.188:3000/api/v1. Nous les stockons ensuite dans la base de données sous le modèle suivant : 

 ``` javascript 
 const deviceSchema = new mongoose.Schema({
  id: {
    type: String
  }, 
  zone: {
      type: String
  }, 
  temperature: {
      type: [Number]
  },
  humidity: {
      type: [Number]
  },
  wind: {
    type: [Number]
  },
  status: {
    type: String
  }
}); 
 ```

- Afin de conserver un historique des différentes données récoltées mais également permettre un suivi des différents objets connectés, nous interrogeons l'API citée plus haut toutes les minutes afin d'alimenter notre BDD. Actuellement nous interrogeons l'API toutes les minutes et stockons un historique pouvant contenir jusqu'à 20 valeurs. Afin de faciliter les modifications, ces deux valeurs sont modifiables très facilement. Ce sont donc des variables globales, modifiables directement dans le code, dans le fichier server.js lignes 10 et 11 : 

``` javascript 
const  HISTORIC = 20
const  MINUTES = 1
```

Évidemment toutes les variables importantes sont définies en global dans server.js et sont modifiables et adaptables au grès du développeur : 

``` javascript 
const PORT = 8080;
const APISOURCEURL = "http://52.14.112.188:3000/api/v1"
const HISTORIC = 20
const MINUTES = 1
```

-  Ci-dessous une liste des différentes routes de notre API ainsi que leur fonction : 

``` javascript 
/devices // renvoie un tableau de tout les devices ou chaque objet de ce tableau
// correspond à un devices avec toutes ses propriétés 
//(humidity, temperature, wind, status, zone et ID
/devices/name // renvoie un tableau contenant la liste des noms de tout les devices
/switch/:device_id // permet de switch le statut d'un device en particulier et
// renvoie le nouveau statut
/device/:device_id // permet d'obtenir les données d'un device en particulier 
/device/:device_id/temperature // permet d'obtenir l'historique des températures 
// d'un device en particulier
/device/:device_id/wind // permet d'obtenir l'historique de la vitesse du vent 
// d'un device en particulier
/device/:device_id/humidity // permet d'obtenir l'historique de l'humidité
// d'un device en particulier
```
Toutes les routes ci-dessus sont des requetes de type GET. Les routes contenant ```:device_id``` prennent en paramètre l'id du device sur lequel nous voulons récupérer des données. 

# UI

Nous utilisons le framework ReactJs pour l'interface : https://fr.reactjs.org/. Il utilise principalement un système de composants (une classe, div, li, Map, Table, etc). Afin de rendre les requêtes à notre API plus facile, nous avons utilisé Axios. Concernant la partie style, nous avons choisi d'utiliser un framework CSS très populaire : Material UI.

## App 

Ce composant est le point d'entrée de notre application et définit le router qui permet la liaison avec les différents composants créés, qui correspondent chacune à un élément du menu : 
- Home 
- Tab (Tableau de données)
- Streetmap (Carte de devices)
- Chart (Graphique d'évolution des devices)

## Home 

C'est la première page qui s'affiche lorsque l'on va à l'url [http://localhost:8080/](http://localhost:8080/ "http://localhost:8080/") ou  [http://192.168.99.100:8080/](http://192.168.99.100:8080/ "http://192.168.99.100:8080/") selon l'OS utilisé. Ce composant liste les différentes fonctionnalités de notre BackOffice.

## Tableau de données

Affiche dans un tableau les différents devices et les informations récupérées : 
- statut (exprimé par un voyant vert s'il est actif, rouge si inactif) 
- température actuelle (°C)
- moyenne des températures stockées
- humidité (%) 
- moyenne des humidités stockées 
- vent (km/h)
- moyenne des vents stockés 

Pour récupérer les informations mises à jour, il faut à chaque fois recharger la page. En cliquant sur le statut, celui-ci peut être modifié. Il devient inactif s'il était actif et vice-versa.

**IMPORTANT** :  Nous avons développé une fonctionnalité qui prends en compte le statut lors du stockage des données. Si dans le composant Tableau de données, je change le statut d'un device à OFF, ses données ne seront pas relevées et donc non stockées en base.

## Carte de devices 

Ce composant affiche une carte, centrée sur Bordeaux, qui affiche les différents devices, positionnés par rapport à leur latitude et longitude respectives. En cliquant sur l'un des devices, celui-ci fera apparaître un popup qui montre les dernières informations récupérées au moment du chargement de la page, c'est à dire : 

- la température actuelle 
- le pourcentage d'humidité 
- la vitesse du vent 
- son statut 

Il utilise l'API React-Leaflet https://react-leaflet.js.org/ qui a été adapté spécialement pour React et qui est gratuite.

## Graphique d'évolution des devices

Ce composant permet un suivi des différents devices via l'affichage d'un graphique de données. L'utilisateur choisi dans une liste déroulante un device ainsi qu'une catégorie et clique sur ```Actualiser un device```. Cela actualisera donc le graphique en dessous en fonction des choix de l'utilisateur. 