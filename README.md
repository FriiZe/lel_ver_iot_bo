﻿# Projet IOT Backoffice - Leoni Laetitia & Vergé Romain

Vous pouvez voir l'énoncé du projet à l'adresse suivante : [http://52.14.112.188:3000/](http://52.14.112.188:3000/ "http://52.14.112.188:3000/")


# Choix des technologies

Pour ce projet orienté web, nos choix se sont basés sur des outils avec lesquels nous avons déjà travaillé et avec lesquels nous sommes à l'aise.

## ReactJS

Nous avons choisi d'utiliser ReactJS pour la partie Front du projet pour plusieurs raisons : 
- D'une part, le système de composants de React ainsi que la réactivité du "state" (affichage réactif et dynamique) se prête parfaitement à l'énoncé du projet.
- D'autre part, nous avons déjà mené différents projets avec ce framework ce qui nous permettra de commencer avec un bagage plus ou moins solide sur cette technologie.
- Ensuite, de nombreux modules sont disponibles afin de faciliter une communication lisible et facile avec une API (AJAX, Axios, ...)
- Pour continuer, ReactJS étant créé et maintenu par Facebook il nous assure une bonne stabilité (environ 10 commits par jour). Il possède également une très forte communauté
- Enfin la possibilité d'utiliser différents frameworks CSS (Bootstrap, Material UI, ...) afin de ne pas perdre de temps sur la partie visuelle.

## ExpressJS

Afin de requêter les différentes routes mises à disposition par notre super professeur mais aussi de créer nos propres routes au sein de notre API, nous utiliserons ExpressJS.
Basé sur NodeJS, et possédant de nombreux atouts (clarté et lisibilité, gestion de l'asynchrone, système de middlewares puissant, ...), notre API sera donc composée de routes permettant de traiter les fonctionnalités décrites plus bas.

## Base de données

Afin de stocker les différentes données nécessaires au fonctionnement de notre BackOffice, nous utiliserons la plus connue des bases de données de type document en NoSQL : MongoDB.
Cette base de données étant orientée document, elle nous permettra sans aucun problème, de gérer les différentes données que nous avons à traiter. 

# Fonctionnalités

- Afficher le temps qu'il fait sur chaque device
- Afficher un voyant vert ou rouge selon si le device est allumé ou éteint avec possibilité de l'éteindre ou de l'allumer à distance
- Afficher les différents relevés groupés par device (vent, humidité, et température) 
- Filtres : lieu le plus chaud, le plus humide, ... 
- Dans le cas ou un device est down, ses températures ne seront pas stockes 
- Graphique d'évolution de device

# Lancer le docker react 

- Dans le dossier iot/react-express-mongo-app faire : docker-compose up --build
- ouvrir firefox et aller a l'adresse suivante : localhost

# Pour lancer depuis Windows avec Docker Toolbox

Lancer Docker Toolbox

Dans react-express-mongo-app/ui/src/components/map.js
et   react-express-mongo-app/ui/src/components/tab.js, changer

```javascript
const apiUrl = `http://localhost:8080`;
```

par

```javascript
const apiUrl = `http://192.168.99.100:8080`;
```

Dans la console faire:

```console
cd react-express-mongo-app
docker-compose build
docker-compose up
```

Ensuite ouvrir son navigateur et aller à l'adresse suivante: 
http://192.168.99.100/